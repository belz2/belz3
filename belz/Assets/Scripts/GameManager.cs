﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public Transform[] spawnPoints;
    public GameObject squareBrick;
    public int numberOfBricksToStart;
    public int level;
    public List<GameObject> bricksInScene;
    public List<GameObject> ballsInScene;
    private ObjectPool objectPool;
    public GameObject extraBall;
    public int numberOfExtraBallsInRow = 0;//ball powerups in each row

	// Use this for initialization
	void Start () {
        objectPool = FindObjectOfType<ObjectPool>();
        int numberOfBricksCreated = 0;
        level = 1;
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if (numberOfBricksCreated < numberOfBricksToStart) // get the number of bricks to spawn at the start
            {
                int bricksToCreate = Random.Range(0, 1); // randomly create the bricks
                if (bricksToCreate == 0) {
                    bricksInScene.Add(Instantiate(squareBrick, spawnPoints[i].position, Quaternion.identity)); // create square brick on each of the spawnpoints
                //} else if (bricksToCreate == 2 && numberOfExtraBallsInRow == 0 ) {//extra ball
                    //bricksInScene.Add(Instantiate(extraBall, spawnPoints[i].position, Quaternion.identity));
                   // numberOfExtraBallsInRow++;
                }// else empty space
            }
        }
        numberOfExtraBallsInRow = 0;//reset for next row
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Use to Create a new object
    public void PlaceBricks() {
        level++;
        foreach (Transform position in spawnPoints)
        {
            int bricksToCreate = Random.Range(0, 2); // randomly create bricks
            if (bricksToCreate == 0)
            {
                GameObject brick = objectPool.GetPooledObject("Square Brick");
                bricksInScene.Add(brick);
                if (brick != null)
                {
                    brick.transform.position = position.position;
                    brick.transform.rotation = Quaternion.identity;
                    brick.SetActive(true);
                }
            }
            else if (bricksToCreate == 2 && numberOfExtraBallsInRow == 0) {
                GameObject ball = objectPool.GetPooledObject("Extra Ball Powerup");
                bricksInScene.Add(ball);
                if (ball != null) {
                    ball.transform.position = position.position;
                    ball.transform.rotation = Quaternion.identity;
                    ball.SetActive(true);
                }
                numberOfExtraBallsInRow++;
            }
        }//end for
        numberOfExtraBallsInRow = 0;
    }
}
