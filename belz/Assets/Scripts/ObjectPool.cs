﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class ObjectPoolItem{
    public GameObject objectToPool;//object we are pulling
    public int amountToPool;//how many we're pulling
}

public class ObjectPool : MonoBehaviour {

    public List<GameObject> pooledObjects;//objects after we create them
    public List<ObjectPoolItem> itemsToPool;

	// Use this for initialization
	void Start () {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool) {
            for (int i = 0; i < item.amountToPool; i++) {
                GameObject obj = (GameObject)Instantiate(item.objectToPool);//cast into game object
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Used to bring a game object from the pull returns game object or null
    public GameObject GetPooledObject(string tag) {//use tag to know which object is being returned
        for (int i = 0; i < pooledObjects.Count; i++) {//goes through all objs
            if (pooledObjects[i].activeInHierarchy == false && pooledObjects[i].tag == tag) {//if object is inactive in the heiarchy and has correct tag
                return pooledObjects[i];
            }
        }
        return null;
    }
}
