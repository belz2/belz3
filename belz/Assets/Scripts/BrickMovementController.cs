﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickMovementController : MonoBehaviour {

    public enum brickState
    {
        stop,move
    }

    public brickState currentState;
    private bool hasMoved;

	// Use this for initialization
	void Start () {
        hasMoved = false;
        currentState = brickState.stop;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentState == brickState.stop) // don't have to do this , but just to make sure
        {
            hasMoved = false;
        }
		if(currentState == brickState.move)
        {
            if (hasMoved == false)// if the move bricks move one time, then stop by setting the hasMoved to true
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - 1); // while the hhe brickstate is moving then move down one level byt setting the position
                currentState = brickState.stop; // then set the brick to stop right away.
                hasMoved = true;
            }
        }
	}
}
