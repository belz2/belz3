﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraBallManager : MonoBehaviour {

    private BallController ballController;
    private GameManager gameManager;
    public float ballWaitTime;//float for fraction of a section 
    private float ballWaitTimeSeconds;
    public int numberOfExtraBalls;
    public int numberOfBallsToFire;
    public ObjectPool objectPool;
    public Text numberOfBallsText;


	// Use this for initialization
	void Start () {
        ballController = FindObjectOfType<BallController>();
        gameManager = FindObjectOfType<GameManager>();
        ballWaitTimeSeconds = ballWaitTime;
        numberOfExtraBalls = 0;
        numberOfBallsToFire = 0;
        numberOfBallsText.text = "" + 1;
	}
	
	// Update is called once per frame
	void Update () {
        //continuously checking num of balls on scene
        numberOfBallsText.text = "" + (numberOfExtraBalls + 1);
        if (ballController.currentBallState == BallController.ballState.fire) {
            if (numberOfBallsToFire > 0) {
                ballWaitTimeSeconds -= Time.deltaTime;//last time method was called
                if (ballWaitTimeSeconds <= 0) {
                    GameObject ball = objectPool.GetPooledObject("Extra Ball");
                    if (ball != null) {
                        ball.transform.position = ballController.ballLaunchPosition;//reference from ball controller
                        ball.SetActive(true);
                        gameManager.ballsInScene.Add(ball);
                        ball.GetComponent<Rigidbody2D>().velocity = 12 * ballController.tempVelocity;//setting velocity of extra to the new ball
                        ballWaitTimeSeconds = ballWaitTime;
                        numberOfBallsToFire--;
                    }
                    //only activated if object is not null
                    ballWaitTimeSeconds = ballWaitTime;
                }
            }
            if (ballController.currentBallState == BallController.ballState.endShot) {
                numberOfBallsToFire = numberOfExtraBalls;
            }
        }

	}
}
