﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Collision of the Ball and bricks
public class AddBall : MonoBehaviour {

    private ExtraBallManager extraBallManager;

	// Use this for initialization
	void Start () {
        extraBallManager = FindObjectOfType<ExtraBallManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.tag == "Ball") {//Condition object with Ball tag collides with powerup
            //add extra ball to count
            extraBallManager.numberOfExtraBalls++;
            this.gameObject.SetActive(false);//when ball collides disable 
        }
    }

}
